public class Empleado {

    // atributos

    private String cedula;
    private String nombre;
    private String puesto;

    // constructores

    public Empleado() {
        this.cedula="";
        this.nombre="";
        this.puesto="";
    }

    public Empleado(String cedula, String nombre, String puesto) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.puesto = puesto;
    }

    // métodos de acceso get y set

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    // toString()

    @Override
    public String toString() {
        return "Empleado{" +
                "cedula='" + cedula + '\'' +
                ", nombre='" + nombre + '\'' +
                ", puesto='" + puesto + '\'' +
                '}';
    }
}
